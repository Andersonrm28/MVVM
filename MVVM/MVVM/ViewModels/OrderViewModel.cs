﻿using GalaSoft.MvvmLight.Command;
using MVVM.Moldes;
using MVVM.Services;
using System;
using System.Windows.Input;

namespace MVVM.ViewModels
{
    public class OrderViewModel : Order
    {
        #region Atributes
        private ApiService apiService;
        private DialogService dialogService;
        #endregion

        #region Constructors
        public OrderViewModel()
        {
            dialogService = new DialogService();
            apiService = new ApiService();
            DeliveryDate = DateTime.Today;
        }
        #endregion

        #region Commands
        public ICommand SaveCommand
        {
            get
            {
                return new RelayCommand(Save);
            }
        }

        private async void Save()
        {
            try
            {
                var order = new Order
                {
                    Id = Id,
                    Client = this.Client,
                    CreationDate = DateTime.Now,
                    DeliveryDate = this.DeliveryDate,
                    DeliveryInformation = this.DeliveryInformation,
                    Description = this.Description,
                    IsDelivered = false,
                    Title = this.Title,
                };

                await apiService.CreateOrder(order);
                await dialogService.ShowMessage("Información", "El pedido ha sido creado");

            }
            catch
            {
                await dialogService.ShowMessage("Error", "Ha ocurrido un error inesperado");
            }
        }
        #endregion


    }
}
